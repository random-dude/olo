#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  
#  Copyright 2020 Reso-nance Numérique <laurent@reso-nance.org>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
import liblo, threading, time, gevent
from datetime import datetime, timedelta

import UI, audioFiles

listenPort = 9000
OSCsendPort = 8000
server = None
listenToOSC = True
runTimeoutThread = True
knownSensors = {}
timeout = 2 # time in seconds after which the sensor will be marked as offline if no data is received
timeoutThreadDelay = 5 # every X seconds, the timeout thread will check if each device is still active

class Sensor:
    """ every ESP8266 is assigned to a Sensor object which manage all communication between the ESP and the server"""

    def __init__(self, hostname, IPaddress, OSCpath, sensorCount):
        self.hostname = hostname
        self.IP = IPaddress
        self.OSCpath = OSCpath
        self.sensorCount = sensorCount
        self.values = [0]*sensorCount
        self.isActive = True
        self.ledON = True
        self.assignedTracks = [None]*sensorCount
        self.expires = datetime.now()

    def calibrateAmbiant(self):
        sendOSC(self.IP, "/calibrateAmbiant")
        print("calibrating ambiant for", self.hostname)

    def calibrateMax(self):
        sendOSC(self.IP, "/calibrateMax")
        print("calibrating max for", self.hostname)

    def getID(self):
        sendOSC(self.IP, "/sendID")
        print("asked", self.hostname, "for ID")

    def toggleLED(self):
        if self.ledON : sendOSC(self.IP, "/ledOFF")
        else : 
            sendOSC(self.IP, "/ledON") # our values may have changed while the LED was OFF
            self.setValues(self.values, refresh=True)
        self.ledON = not self.ledON
        print("set", self.hostname,"LED to", "ON" if self.ledON else "OFF")

    def setActive(self):
        self.isActive = True
        self.expires = datetime.now() + timedelta(seconds=timeout)
        print("sensor", self.hostname, "reconnected")
        UI.toast(f"le capteur {self.hostname} est à nouveau en ligne", "capteur reconnecté :", "info")
        UI.refreshSensorsModal()
        self.findAssignation()

    def setValues(self, values, refresh=False):
        assert len(values) == self.sensorCount, "received {} values for {} sensors : {}".format(len(values), self.sensorCount, values)
        for i in range(self.sensorCount) :
            trackName = self.assignedTracks[i]
            if trackName and self.ledON and ( values[i] != self.values[i] or refresh ):
                try : 
                    track = next(t for t in audioFiles.currentSong.tracks if t.name == trackName)
                    if track.status == "auto" : track.setVolume(values[i]/1000)
                except StopIteration : findAssignation(self)
        self.values = values
        self.expires = datetime.now() + timedelta(seconds=timeout)
        if not self.isActive : self.setActive()

    def findAssignation(self):
        """ assign every value of this sensor to a track in current song either by default or as previously assigned"""
        for i, _ in enumerate(self.values): findAssignation(self.hostname+str(i))
        self.setValues(self.values, refresh=True)
        
    def toDict(self):
        return {k:v for k,v in self.__dict__.items() if not k.startswith("_") and not k=="expires"}

def listen():
    """this blocking function contains the OSCserver. It will fire callbacks each time an OSC message is received.
    To stop the server, listenToOSC must be set to False"""
    global server
    try:
        server = liblo.ServerThread(listenPort)
        print("listening to incoming OSC on port %i" % listenPort)
    except liblo.ServerError as e:
        print(e)
        raise SystemError(f"unable to start the OSCserver : {e}")
        
    server.add_method("/myID", None, handleID)
    server.add_method(None, None, incomingOSC) # will match every other OSC messages
    server.start()

def sendOSC(IPaddress, command, args=None):    
    """ this function wraps the OSC path and optionals arguments into an OSC message and sends it to the provided IP address via UDP
    :param IPaddress: the destination IPaddress as a String : "127.0.0.1"
    :param command: the osc path as a String : "/test"
    :param args: optional list of arguments, all types accepted
    """
    if args :
        print("sending OSC {} {} to {}".format(command, args, IPaddress))
        liblo.send((IPaddress, OSCsendPort), command, *args)
    else :
        print("sending OSC {} to {}".format(command, IPaddress))
        liblo.send((IPaddress, OSCsendPort), command)

def broadcastOSC(command, args=None):
    """ since we do not have the permission to send to broadcast, this message will be sent to every known sensor"""
    for sensor in knownSensors.values():
        if args : sendOSC(sensor.IP, command, args)
        else : sendOSC(sensor.IP, command)

def handleID(path, args, types, src):
    """ this callback is fired when the OSC server receives "/myID". It will add a new Sensor to the knownSensors dict if it ain't already in it"""
    global knownSensors
    try : OSCpath, hostname, IPaddress, sensorCount = args
    except ValueError : 
        print("ERROR : /myID should contains 4 args : OSCpath, hostname, IPaddress, sensorCount")
        print("        %i args received :" % len(args), args)
        return
    assert src.hostname == IPaddress, "IP address from /myID doesn't match sender's IP"
    if hostname in knownSensors : 
        print("received ID from already known sensor", hostname)
        knownSensors[hostname].setActive()
    else : 
        knownSensors.update({hostname:Sensor(hostname, IPaddress, OSCpath, sensorCount)})
        print("added new sensor", hostname, OSCpath, "@", IPaddress)
        UI.toast(f"{hostname}, addresse OSC :{OSCpath} @ {IPaddress}", "nouveau capteur ajouté :", "success")
        UI.refreshSensorsModal()
        knownSensors[hostname].findAssignation()

def incomingOSC(path, args, types, src):
    """ this callback is fired when the OSC server receives anything else than "/myID".
    It will update known sensors values or ask the sender to identify itself"""
    try :
        sensor = next(s for s in knownSensors.values() if s.OSCpath == path and s.IP == src.hostname)
        sensor.setValues(args)
    except StopIteration : 
        sendOSC(src.hostname, "/sendID")
        print(f"got message {path} from unknown sensor @ {src.hostname}, asked for ID")
        for a, t in zip(args, types): print (f"  argument of type {t}: {a}")

def checkDevicesForTimeout():
    """ this blocking function periodically checks for devices which haven't send value for more than the timeout delay"""
    while runTimeoutThread : 
        currentTime = datetime.now()
        for sensor in knownSensors.values() :
            if not sensor.isActive : sensor.getID()
            elif currentTime > sensor.expires : 
                print("sensor", sensor.hostname,"marked as inactive")
                UI.toast(f"le capteur {sensor.hostname} ne semble plus répondre", "capteur déconnecté :", "danger")
                sensor.isActive = False
                UI.refreshSensorsModal()
                sensor.getID()
        gevent.sleep(timeoutThreadDelay)

def startTimeoutThread():
    """helper function to launch the timeout checking thread"""
    gevent.spawn(checkDevicesForTimeout)

def getKnownSensors():
    """ returns a list of (serialisable) dict for every known sensor, sorted by hostname"""
    sensors = [s.toDict() for s in knownSensors.values()]
    return sorted(sensors, key=lambda k: k["hostname"]) 

def findAssignation(sensorID):
    """ this function will look in currentSong for tracks listing the provided sensor as their prefered assignation. If one is available, it will
    assign this track to it's value. If none is found, it will try to assign itself as a (temporary) default assignation on any not-assigned track.
    If none is found, then it will notify the user and stay unassigned
    :param sensorID: (str) hostname of the sensor + value index, ex: myDevice0"""
    sensorHostname, valueIndex = sensorID[:-1], int(sensorID[-1])
    if audioFiles.currentSong is None : return # no song loaded = nothing to assign
    if sensorHostname not in knownSensors or knownSensors[sensorHostname].isActive == False : # sensor is offline or unknown
        for track in [t for t in audioFiles.currentSong.tracks if t.defaultAssignement == sensorID]:
            track.defaultAssignement = None # clear default assignement
            if sensorHostname in knownSensors : knownSensors[sensorHostname].assignedTrack = None
        return
    sensor = knownSensors[sensorHostname]
    try : # the sensor has one manual assignation
        assignedTrack = next(t for t in audioFiles.currentSong.tracks if t.assignedTo == sensorID)
        sensor.assignedTracks[valueIndex] = assignedTrack.name
        UI.toast(f"la piste {assignedTrack.HRname} est maintenant contrôlé par le capteur n°{valueIndex+1} de {sensorHostname}", "assignation effectuée ", "secondary", delay=3)
        print(f"assigned track {assignedTrack.HRname} to the value #{valueIndex} of {sensorHostname}")
        if assignedTrack.defaultAssignement and assignedTrack.defaultAssignement != sensorID : # track has previously been assignated to another sensor/value
            defaultAssignement = assignedTrack.defaultAssignement
            assignedTrack.defaultAssignement = sensorID # will be assigned by default as well
            findAssignation(defaultAssignement) # automatically re-assign this sensor that was assignated by default
    except StopIteration : # no manual assignation on this song
        try : # if there is tracks not currently assigned to a sensor
            unused = lambda t : t.assignedTo not in [s for s in knownSensors.values() if s.isActive] and t.defaultAssignement is None
            sensorLessTrack =  next(t for t in audioFiles.currentSong.tracks if unused(t))
            # sensorLessTrack =  next(t for t in audioFiles.currentSong.tracks if t.assignedTo is None and t.defaultAssignement is None)
            sensorLessTrack.defaultAssignement = sensorID
            sensor.assignedTracks[valueIndex] = sensorLessTrack.name
            UI.toast(f"la piste {sensorLessTrack.HRname} est à présent contrôlé par le capteur n°{valueIndex+1} de {sensor.hostname}", "assignation par défaut ", delay=2)
            print(f"default assigned track {sensorLessTrack.HRname} to sensor {sensor.hostname}")
            if sensorLessTrack.assignedTo is None : 
                sensorLessTrack.assignedTo = sensorID # automatically create an assignation so it will stay the same next time
                audioFiles.currentSong.saveToFile()
        except StopIteration : # no free tracks left
            UI.toast(f"le capteur {sensor.hostname} n'est assigné à aucune piste", "aucune piste libre ", "warning", 10)
            print(f"no assignations availables for sensor {sensor.hostname}")
            sensor.assignedTrack[valueIndex] = None
            return
    UI.sensorModalUpdateAssignations()

def autoAssign():
    """ perform auto-assignation on every value of every sensor"""
    if audioFiles.currentSong :
        activeSensors = [s for s in knownSensors.values() if s.isActive]
        print(f"auto assign sensors {' '.join([s.hostname for s in activeSensors])} to song {audioFiles.currentSong.HRname}")
        for sensor in activeSensors : sensor.findAssignation()
        for t in audioFiles.currentSong.tracks : # silence unassigned tracks
            if not (t.defaultAssignement or t.assignedTo) and t.status == "auto" : t.setVolume(0)
        debugPrintAssignations()

def debugPrintAssignations(): # FIXME : debug
    # print in column left aligned
    getColWidth = lambda x: max(len(word) for row in x for word in row) + 2
    printData = lambda x : [print("\t" + "".join(word.ljust(getColWidth(x)) for word in row)) for row in x]
    # print assignations per track
    debugData = [("track name", "assignation", "default")] 
    for t in audioFiles.currentSong.tracks: debugData+=[(t.HRname, str(t.assignedTo), str(t.defaultAssignement))]
    printData(debugData)
    # print assignations per sensor
    debugData = [("sensorName", "value #", "assigned to")]# assignations per sensor
    for s in  knownSensors.values():
        for i, _ in enumerate(s.values) : debugData+=[(s.hostname,str(i),s.assignedTracks[i].split("/")[-1])]
    print()
    printData(debugData)

def learnSensor(trackName, timeout=5):
    """ this function sums up the value of active sensors and returns the sensor hostname+valueIndex with the highest sum
    :param timeout: time period in seconds during which the value will be summed"""
    activeSensors = [s for s in knownSensors.values() if s.isActive]
    sumSensorData= {(s.hostname,i):0 for s in activeSensors for i in range(len(s.values))} #{ (sensor.hostname, valueIndex) : sum, ...}
    timeStarted = datetime.now() 
    while datetime.now() < timeStarted + timedelta(seconds=timeout) :
        for sensor in [s for s in knownSensors.values() if s.isActive] :
            for i in range(len(sensor.values)) :
                sumSensorData[(sensor.hostname, i)] += sensor.values[i]
        gevent.sleep(.05)
    sensorHostname, valueIndex = sorted(sumSensorData.items(), key=lambda kv: kv[1])[0][0] # will return a tuple (sensor.hostname, valueIndex)
    UI.socketio.emit("closeAssignationModal", namespace="/home")
    UI.sensorAssign(sensorHostname+str(valueIndex), trackName)
