#!/usr/bin/env python
# -*- coding: utf-8 -*-
from distutils.core import setup
from Cython.Distutils.extension import Extension
from Cython.Distutils import build_ext

ext_modules = [
    Extension("main",  ["main.py"]),
    Extension("sensors",  ["sensors.py"]),
    Extension("UI",  ["UI.py"]),
    Extension("pd",  ["pd.py"]),
    Extension("audioFiles",  ["audioFiles.py"]),
]

for e in ext_modules : e.cython_directives = {"language_level": "3"}

setup(
    name = 'olo',
    cmdclass = {'build_ext': build_ext},
    ext_modules = ext_modules,
)
