$( document ).ready(function() {
    var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + '/songs');
    var songsInfos = [];

    $(".modal").hide();
    $(".template").hide();
    $('.toast').toast('show');

    // ---------------------- MAIN PAGE ----------------------

    // redirects home when the back button is clicked
    $(document).on("click", ".button-back", function(event){
        window.location.replace("/");
    });

    // every .closemodal elements will close the specific modal whose ID is stored in data-modal
    $(document).on("click", ".closeModal", function(event){
        const modalID = $(event.target).data("modal");
        $("#"+modalID).modal("hide");
    });

    // display tooltips which may have been modified or created
    $(document).on("DOMSubtreeModified", "#cards-container, #song-editor", function(event){
        $('[data-toggle="tooltip"]').tooltip();
    });

    // debug
    $(document).on("click", ".fa-plus-square", function(event){
        const name = Math.random().toString(36).slice(2); //random alphanumerical ID
        editSong({HRname:"nouveau morceau",name:name, tracks:[]}, undo=true);
        console.log(songsInfos);
    });

    // start the download of a song
    $(document).on("click", ".fa-download", function(event){
        const songName = $(this).closest(".card").attr("data-name");
        socket.emit("downloadSong", songName);
        $("#loadingSongName").text(songName);
        $("#loadingFile").modal();
    });

    // delete song : ask for confirmation
    $(document).on("click", ".ask-confirmation", function(event){
        const songName = $(this).closest(".card").attr("data-name");
        console.log(songsInfos)
        const song = songsInfos.filter(function(s){
            return encodeURIComponent(s.name) == songName
        })[0];
        $("#delete-song-name").text(song.HRname+" ");
        $("#delete-song-name").attr("data-name",songName);
        $("#delete-warning").modal();
    })

    // really delete the song
    $(document).on("click", ".delete-song", function(event){
        const songName = $("#delete-song-name").attr("data-name");
        socket.emit("deleteSong", songName);
        $("#delete-warning").modal("hide");
        $('.card[data-name="'+songName+'"').remove();
    })


    // ---------------------- SONG EDIT ----------------------

    // edit song : open modal
    $(document).on("click", ".edit-this-song", function(event){
        const songName = $(this).closest(".card").attr("data-name");
        const thisSong = songsInfos.filter(function(song){
            return encodeURIComponent(song.name) == songName
        })[0];
        editSong(thisSong);
    })

    // trigger the hidden file-input
    $(document).on("click", "#upload-image", function(event){
        $("#image-fileinput").click();
    });

    // upload the file when selected
	$(document).on('change', '#image-fileinput', function(event) {
        const successCallback = function() { $("#upload-image").after('<span class="fas fa-check" style="color:green">');}
        const failedCallback = function(postRequest) { $("#upload-image").after('<span class="fas fa-times" style="color:red">');}
        sendFormData($(this).closest('form')[0], successCallback, failedCallback);
     });

     // toggle icon representing track types when clicked
     $(document).on("click", ".sample-track", function(event) {
         const songName = $(this).closest(".row").attr("data-song-name");
         const trackName = $(this).closest(".row").attr("name");
         if ($(event.target).hasClass("fa-bell")) {
             $(event.target).removeClass("fa-bell").addClass("fa-music");
             socket.emit("isSampleChanged", songName, trackName, false);
         }
         else if ($(event.target).hasClass("fa-music")) {
            $(event.target).removeClass("fa-music").addClass("fa-bell");
            socket.emit("isSampleChanged", songName, trackName, true);
         }
     });

     // delete track
     $(document).on("click", ".remove-track", function(event) {
        const songName = $(this).closest(".row").attr("data-song-name");
        const trackName = $(this).closest(".row").attr("name");
         socket.emit("deleteTrack",songName, trackName)
         $(this).closest(".row").remove();
     });

     // move track
     $(document).on("click", ".move-track", function(event) {
        const songName = $(this).closest(".row").attr("data-song-name");
        const trackName = $(this).closest(".row").attr("name");
         socket.emit("moveTrack",songName, trackName, $(this).attr("data-direction"))
     });
    
     // trigger hidden input file to upload wav when .upload-track is clicked
    $(document).on("click", ".upload-track", function(event){
        $(this).parent().find('input[type="file"]').click();
     });

    // upload the wav file for an existing track
	$(document).on('change', '.audio-fileinput', function(event) {
        const uploadButton = $(this).parent().parent().find(".upload-track");
        const successCallback = function() { 
            $(uploadButton).after('<span class="fas fa-check" style="color:green">');// add green tick mark next to the button
            $(uploadButton).css("color","") // reset it's color
            $(this).parent().parent().find(".edit-track-name").removeAttr("disabled"); // make track name editable again
            // console.log($(this).parent().parent().find("edit-track-name"));
        }
        const failedCallback = function(postRequest) { 
            // add a red cross next to the button
            $(uploadButton).after('<span class="fas fa-times" style="color:red">');
        }
        sendFormData($(this).closest('form')[0], successCallback, failedCallback);
     });

    // upload the wav file for a new track
	$(document).on('change', '#new-track-input', function(event) {
        // since the modal refresh will be handled server-side by websockets, we won't use callbacks
        const successCallback = function() {}
        const failedCallback = function(postRequest) {}
        sendFormData($("#new-track-form")[0], successCallback, failedCallback);
     });

      // add a new track by uploading a wav file
      $(document).on("click", ".add-track", function(event){
        $("#new-track-input").click();
    });

     // edit track name
     $(document).on("blur", ".edit-track-name", function(event){
         const songName = $(this).closest(".row").attr("data-song-name");
         const oldName = $(this).closest(".row").attr("name");
         let newName = $(event.target).val();
         if (newName != oldName) {
            //  newName = linuxPathFriendly(newName);
            //  $(event.target).val(newName);
             socket.emit("trackNameChanged", songName, oldName, encodeURIComponent(newName));
             console.log("changed track name for song", songName, oldName,"->", newName)
         }
     });

     // edit song name
     $(document).on("blur", "#song-editor-text-edit", function(event){
        const songName = $("#song-editor input[name='songName']").val();
        let newName = $(event.target).val();
        socket.emit("songNameChanged", songName, encodeURIComponent(newName));
        console.log("changed song name from",decodeURIComponent(songName),"->", newName)
     });

     // reset the upload zip modal and show it
     $(document).on("click", "#import-button", function(event){
        $("#zip-import-loading").hide();
        $("#zip-error").hide();
        $("#zip-import-loading").find("h4").text('téléversement en cours, merci de patienter ...');
        $("#zip-import").modal();
     });

    // open the file input
	$(document).on('click', "#upload-zip", function(event) {
        $(this).parent().find('input[type="file"]').click();
    });

    // submit the form and provide info on the processing
	$(document).on('change', "#zip-input", function(event) {
        $("#zip-error").hide();
        $("#zip-import-loading").show();
        const successCallback = function() { 
            $("#zip-import-loading").find("h4").text('téléversement terminé, traitement du zip en cours ...');
        }
        const failedCallback = function(postRequest) { 
           $("#zip-import-loading").hide();
           $("#zip-error").show();
        }
        sendFormData($("#zip-upload-form")[0], successCallback, failedCallback); // FIXME callbacks should accept error code as args
     });


    // ---------------------- WEBSOCKETS CALLBACKS ----------------------
    
    // generates cards for each song and put them in a responsive deck
    socket.on("songsInfos", function (data){
        songsInfos = data
        console.log("generating cards for songs :", data);
        let songCount = 0;
        data.forEach(function(song) {
            updateSong(song);
            songCount ++;
            // we need to add breaks depending on the viewport size from sm to xl
            if (songCount%2 == 0) $("#cards-container").append('<div class="w-100 d-none d-sm-block d-md-none">')
            if (songCount%3 == 0) $("#cards-container").append('<div class="w-100 d-none d-md-block d-lg-none">')
            if (songCount%4 == 0) $("#cards-container").append('<div class="w-100 d-none d-lg-block d-xl-none">')
            if (songCount%5 == 0) $("#cards-container").append('<div class="w-100 d-none d-xl-block">')
        });
    });

    // when the song folder has been zipped and is ready to download
    socket.on("downloadReady", function(path){
        console.log("download ready", path);
        $("#loadingFile").modal("hide");
        window.open(path); // start download on another window
    });

    // the uploaded zip has been processed
    socket.on("zipUploadSuccess", function(song){
        console.log("successfully added song from zip");
        $("#zip-import").modal("hide");
        updateSong(song);
    });

    socket.on("updateSong", function(song){
        songsInfos.forEach(function (s){
            if (s.name === song.name) songsInfos[songsInfos.indexOf(s)] = song; // update song if already in array
        });
        if (songsInfos.filter(function(s){return s.name == song.name}).length==0) songsInfos.push(song); // else append it
        updateSong(song);
    });

    // when a track has been added
    socket.on("refreshSongEditor", function(song){
        editSong(song);
        console.log("refreshed song editor for", song.name)
    });


    // ---------------------- FUNCTIONS ----------------------

    // generates a card corresponding to a given song with an image on top, the number of tracks/samples ...
    function generateCard(song){
        let cardTemplate = $(".card.template").clone(); // we start from a hidden template
        cardTemplate.removeClass("template").attr("data-name", encodeURIComponent(song.name)); // which is template no more
        if(song.image) $(cardTemplate).find("img").attr("src","../"+song.image); // the default image is in the template
        $(cardTemplate).find("h5").text(song.HRname);
        // we will count the number of wavs which are samples
        const sampleTracks = song.tracks.filter(function(track) {return track.isSample;});
        const audioTracks = song.tracks.length - sampleTracks.length;
        $(cardTemplate).find(".track-count").text(audioTracks.toString()+" pistes");
        $(cardTemplate).find(".sample-count").text(sampleTracks.length.toString()+" samples");
        return cardTemplate;
    }

    // construct a modal from the template allowing the user to edit everything in a song object
    function editSong(song){
        console.log("creating editor for",song)
        $("#song-editor-title").html("éditer <em>&#171;"+song.HRname+"&#187;</em>"); // so french
        $("#song-editor-text-edit").val(song.HRname); // default new song name
        $("#song-editor input[name='songName']").val(encodeURIComponent(song.name)) // the song name needs to be passed to the server to know where to upload files
        $("#song-editor-tracks").empty();
        if (song.tracks.length > 0) { // new songs doesn't have any tracks
            song.tracks.forEach(function (track){
                trackEditor = generatetrackEditor(track, song.name)
                $("#song-editor-tracks").append(trackEditor);
            });
        }
        else $("#song-editor-tracks").append("<h5>aucune piste pour l'instant"); // used by default song when creating a new one
        $("#song-editor").modal();
    }

    // add generated song card to deck if it doesn't exists or replace the card with the updated one
    function updateSong(song){
        console.log("updating song", song.name);
        name = encodeURIComponent(song.name);
        const updatedCard = generateCard(song);
        if ($('#cards-container .card[data-name="'+name+'"]').length > 0) {// this card already exist
            console.log("replacing", $('#cards-container .card[data-name="'+name+'"]'), "with", updatedCard)
            $('#cards-container .card[data-name="'+name+'"]').replaceWith(updatedCard);
        }
        else $('#cards-container').append(updatedCard);
        $(updatedCard).show();
        if ($("#song-editor").hasClass("show") && $("#song-editor-text-edit").val() == song.HRname) {  // if the song editor modal is opened on this song
            editSong(song); // update it
            console.log("updating song editor modal")
        }
    }

    // returns a DOM element containing every widget needed to edit one track
    function generatetrackEditor(track, songName){
        track.name = encodeURIComponent(track.name);
        songName =  encodeURIComponent(songName);
        let trackEditor = $('<div class="row" name="'+track.name+'" data-song-name="'+songName+'">') // will be used for events
        const upButton=$('<a href="javascript:void(0);"class="fas fa-chevron-up move-track p-1" data-direction="up">');
        const downButton=$('<a href="javascript:void(0);"class="fas fa-chevron-down move-track p-1" data-direction="down">');
        const trackNameEditor = $('<input type="text" class="form-control edit-track-name my-1">').val(track.HRname) // track name editor
        $(trackEditor).append($('<div class="col-2 my-auto mx-auto")>').append(upButton.add(downButton))) // move up and down buttons
        $(trackEditor).append($('<div class="col-7 my-auto")>').append(trackNameEditor))
        const sampleIcon = (track.isSample) ? "bell" : "music"; // bell icon for one shot sample, music note for looped tracks
        $(trackEditor).append($('<div class="col-1 my-auto"><a href="javascript:void(0);" class="sample-track fa fa-'+sampleIcon+'" data-toggle="tooltip" title="piste/sample">')); // isSample track button
        let wavUpload=$('<div class="col-1 my-auto">'); // container for the button and hidden form / file input
        let uploadForm = $('<form  method=POST enctype=multipart/form-data style="display:none;">');
        $(uploadForm).append($('<input type="text" name="songName">').attr("value", songName));
        $(uploadForm).append($('<input type="text" name="trackName">').attr("value", track.name));
        $(uploadForm).append($('<input type="file" class="form-control-file audio-fileinput" name="wavFile" accept=".wav,audio/x-wav">'));
        const uploadButton=$('<a href="javascript:void(0);" class="upload-track fa fa-upload" data-toggle="tooltip" title="importer un wav">'); // upload track button)
        wavUpload.append(uploadForm).append(uploadButton)
        $(trackEditor).append(wavUpload);
        $(trackEditor).append($('<div class="col-1 my-auto"><a href="javascript:void(0);" class="remove-track fa fa-trash" data-toggle="tooltip" title="supprimer">')); // delete track button
        // when a new track is added and the wav file isn't uploaded yet track.isEmpty is set to true
        if (track.isEmpty) $(trackEditor).find(".edit-track-name").attr("disabled", true);
        if (track.isEmpty) $(trackEditor).find(".upload-track").css("color", "lightgreen");
        return trackEditor
    }

    // send the form as a post request to avoid reloading the page
    function sendFormData(form, successCallback, failedCallback) {
        let postRequest = new XMLHttpRequest(); // I could have used $.ajax() instead
        postRequest.open("POST", "songs", true);
        postRequest.onload = function(event) {
            if (postRequest.status == 200) {
                console.log("file uploaded successfully");
                successCallback();
            }
            else {
                console.log("error when uploading file :", postRequest.status);
                failedCallback(postRequest);
            }
        };
        postRequest.send(new FormData(form));
    }

    // returns the input string without illegal characters for a path
    function linuxPathFriendly(text) {
        return text.replace(/[&\/\\#,+()$~%'":*?<>{}@;]/g,'')
    }

    // function shortenText(text, maxLength){
    //     return (text.length > maxLength) ? text.substring(0, maxLength-3)+"..." : text;
    // };
});