$( document ).ready(function() {
    var socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port + '/home');
    var currentSong = NaN;

    $(".modal").hide();
    $("#popover-template").hide();
    $(".sensor-template").hide();
    $('[data-toggle="tooltip"]').tooltip();
    // $('[data-toggle="tooltip"]').tooltip({delay:{show:500, hide:100}});

    //  Toggle the navbar by pushing UIcontainer right
    $(document).on("click", ".toggleNav", function(event){
         if ($("#sidebar").width() == 0) { // sidebar is closed
            $("#sidebar").width("250px");
            $("#UIcontainer").css("margin-left", "250px");
        }
         else {
            $("#sidebar").width("0px");
            $("#UIcontainer").css("margin-left","0px");
            isNavOpen = false;
        }
    });

    // redirect the user to the songs page when the right arrow is clicked
    $(document).on("click", ".button-forward", function(event){
        window.location.replace("/songs");
    })

    // display the confirmation modal when the user clicks on the poweroff button
    $(document).on("click", "#power-off-modal", function(event){
        $("#poweroff-warning").modal("show");
    })

    $(document).on("click", "#power-off", function(event){
        socket.emit("powerOFF");
        window.location.replace("/shutdown");
    })

    // every .closemodal elements will close the specific modal whose ID is stored in data-modal
    $(document).on("click", ".closeModal", function(event){
        const modalID = $(event.target).data("modal");
        $("#"+modalID).modal("hide");
    })

    // load a song when clicked on it
    $(document).on("click", ".song-title", function(event){
        const songName = $(event.target).attr("id");
        console.log("asked to load song", songName);
        socket.emit("loadSong", songName);
    })

    // calibrate ambient for every sensor
    $(document).on("click", "#calibrate-ambiant", function(event){
        console.log("calibrating ambiant for every sensor...")
        socket.emit("calibrateAmbient");
    })

    // open the sensors modal
    $(document).on("click", ".fa-wifi", function(event){
        $("#sensors").modal();
    })

    // toggle the sensor led
    $(document).on("click", ".sensor-led", function(event){
        if ($(this).hasClass("sensor-disabled")) return;
        const hostname = $(this).closest(".sensor").attr("name");
        const ledStatus = $(this).hasClass("fa-toggle-on")
        if (ledStatus) $(this).removeClass("fa-toggle-on").addClass("fa-toggle-off");
        else $(this).removeClass("fa-toggle-off").addClass("fa-toggle-on");
        socket.emit("sensorLed", hostname, ledStatus)
    });

    // calibrate the sensor for max light
    $(document).on("click", ".sensor-calibration", function(event){
        if ($(this).hasClass("sensor-disabled")) return;
        const hostname = $(this).closest(".sensor").attr("name");
        socket.emit("sensorCalibMax", hostname)
    });

    // song play / stop
    $(document).on("click", ".transport-button", function(event){
        const action = $(event.target).attr("data-action");
        console.log(action, "song");
        socket.emit("transportSong", action);
        setPlayPause((action=="play"));
    })

    // track status change
    $(document).on("click", ".status-icon", function(event){
        const trackname = $(this).closest(".track-container").attr("data-track-name");
        // console.log("toggled status for track", trackname);
        socket.emit("toggleStatus", trackname);
    })

    // slider has been touched
    $(document).on("input", 'input[type="range"]', function(event){
        if ($(this).attr("id") === "master-input") socket.emit("sliderTouched", "master", this.value);
        else {
            const trackname = $(this).closest(".track-container").attr("data-track-name");
            // console.log("slider", trackname, "changed to", this.value);
            socket.emit("sliderTouched", trackname, this.value);
        }
    });

    // play the sample track when the bell is clicked
    $(document).on("click", '.fa-bell', function(event){
        const trackname = $(this).closest(".track-container").attr("data-track-name");
        socket.emit("playSample", trackname);
    });

    // insert the trackname and hr into the track assignation modal and show it
    $(document).on("dblclick",".rangeslider--disabled", function(event){
        const trackName = $(event.target).closest(".track-container").attr("data-track-name");
        const trackHRname = $(event.target).closest(".track-container").attr("data-track-HRname");
        $("#track-assignation").find("h4").html("assignation de la piste <strong>"+trackHRname+"</strong>")
        $("#track-assignation").attr("name",trackName)
        $("#assign-help").text("éclairer le capteur que vous souhaitez utiliser pour contrôler cette piste ...").hide()
        $("#track-assignation").modal();
    });

    // ask server to assign a track on a sensor
    $(document).on("click", '.sensor-input-assign', function(event){
        sensorName = $(this).attr("name");
        trackName = $("#track-assignation").attr("name");
        console.log("asked to assign", sensorName, "to", trackName);
        socket.emit("sensorAssign", sensorName, trackName);
        $("#track-assignation").modal("hide")
    });

    // start the sensor-learning process
    $(document).on("click", '.sensor-learn', function(event){
        trackName = $("#track-assignation").attr("name");
        $("#assign-help").show()
        socket.emit("sensorLearn", trackName);
    });

    // when a master status is change, all status-icons will change and the sliders must be updated
    $(document).on("click", ".master-icon", function(event){
        const status = $(this).attr("name") // status is set as the name of the <a>
        socket.emit("masterStatus", status);
        const iconFor = {auto:"fa-volume-up", manual:"fa-user", mute:"fa-volume-mute"}
        $(".status-icon").removeClass(iconFor.auto).removeClass(iconFor.manual).removeClass(iconFor.mute).addClass(iconFor[status])
        if (status === "manual") $(".rangeslider--vertical").removeClass("rangeslider--disabled");
        else $(".rangeslider--vertical").addClass("rangeslider--disabled");
    });

    // updates availables songs names on server request
    socket.on('songList', function(songList) {
        console.log("updating available songs :", songList);
        displaySongs(songList);
    });

    // updates current song tracks on server request
    socket.on('currentSong', function(song) {
        console.log("updating current song :", song);
        if (!song){
            $("#currentSongName").text("aucun morceau chargé")
            $('.transport-button').hide();
            $("#current-song-image").hide();
            $("#no-song-loaded").show();
            if ($("#sidebar").width() == 0) $(".toggleNav").click(); // open the sidebar if closed
        }
        else {
            $("#no-song-loaded").hide()
            currentSong = song;
            displayTracks(song.tracks);
            $("#currentSongName").text(song.HRname);
            $("#current-song-image").attr("src", song.image).show();
            $(".currentSong").removeClass("currentSong"); // select song in sidebar
            $("#"+encodeURIComponent(song.name)).addClass("currentSong");
            song.tracks.forEach(function(track){ // set the volume slider for every track
                updateSlider(track.name, track.volume);
            })
            setPlayPause(song.isPlaying);
        }
    });

    // update the tracks volumes and sensors values
    socket.on('updateUI', function(data) {
        if ($("#sensors").hasClass("show")) { // don't update modal if it's hidden
            Object.keys(data.sensors).forEach(function(hostname) {
                for (let i=0; i<data.sensors[hostname].length; i++){
                    const value = (data.sensors[hostname][i]/10).toString()+"%";
                    $('.sensor[name="'+encodeURIComponent(hostname)+'"]').find('.sensor-value[data-index="'+i.toString()+'"]').text(value);
                };
            });
        }
        Object.keys(data.tracks).forEach(function(trackName){
            updateSlider(trackName, data.tracks[trackName]);
        })

    });

    // update the master volume horizontal slider
    socket.on('masterVolume', function(volume) {
        $('input[type="range"]').rangeslider({ polyfill: false });
        $("#master-input").val(volume).change();
    });

    // update the status icon for a track on server request
    socket.on('changedTrackStatus', function(track) {
        // console.log("updating track", track.name, "status to", track.status);
        const statusIcons = {auto:"fa-volume-up", manual:"fa-user", mute:"fa-volume-mute"};
        let lastClass; // in order to remove the old icon we have to know it's previous class
        if (track.status === "auto") lastClass = statusIcons["manual"];
        else if (track.status === "manual") lastClass = statusIcons["mute"];
        else lastClass = statusIcons["auto"];
        const trackContainer = $('.track-container[data-track-name="'+encodeURIComponent(track.name)+'"]');
        const statusIcon = trackContainer.children(".status-icon");
        statusIcon.removeClass(lastClass).addClass(statusIcons[track.status]);
        // disable the slider when in other mode than manual
        const rangeslider = trackContainer.find('.rangeslider');
        if (track.status === "manual") rangeslider.removeClass("rangeslider--disabled");
        else rangeslider.addClass("rangeslider--disabled");
        // set its value to 0 if muted
        if (track.status === "mute") trackContainer.find('input').val(0).change();
    });

    // update the sensor modal
    socket.on('refreshSensorsModal', function(sensors) {
        console.log("refreshing sensors modal :", sensors)
        $(".sensor").remove();
        if (sensors.length == 0) $("#sensor-container").append($('<div class="text-center py-3 sensor sensor-name">').text("Aucun capteur trouvé"));
        else sensors.forEach( function(sensor){
            addSensorToModal(sensor);
        });
        updateTrackAssignationModal(sensors);
        $('[data-toggle="tooltip"]').tooltip();
    });


    // update the sensor modal
    socket.on('updateSensorAssignation', function(sensors) {
        console.log("updated assignations", sensors);
        sensors.forEach(function (sensor){
            for (let i=0; i<sensor.assignations.length; i++){
                const trackName = shortenText(sensor.assignations[i],  Math.max(70-5*sensor.assignations.length, 10))
                $('.sensor[name="'+encodeURIComponent(sensor.hostname)+'"]').find('.sensor-assignation[data-index="'+i.toString()+'"]').text(trackName);
            };
        });
    });
    
    // socket.on('learnResult', function(sensor) {
    //     console.log("learn results :", sensor.hostname + sensor.valueIndex.toString())
    //     const button = $('<a href="javascript:void(0);" class="sensor-input-assign fas fa-lightbulb">');
    //     button.attr("name", encodeURIComponent(sensor.hostname+sensor.valueIndex.toString()));
    //     button.append("<span>").text("le capteur "+sensor.hostname+" à été séléctionné. Cliquer sur l'icône pour l'assigner à cette piste")
    //     $("#assign-help").html(button)
    // });
    
    // called when the learning process has finished
    socket.on('closeAssignationModal', function() {
        $("#track-assignation").modal("hide");
    });

    function displaySongs(songsList) {
        $("#songs-titles").empty();
        songsList.forEach( function(songTitle){
            const displayTitle = shortenText(songTitle, 15);
            songTitle = encodeURIComponent(songTitle);
            $("#songs-titles").append('<a href="javascript:void(0)" class="song-title" id="'+songTitle+'">'+displayTitle+'</a>')
            const editButton = '<span class="song-edit fas fa-chevron-right" data-song-name="'+songTitle+'"></span>';
            const deleteButton = '<span class="song-delete fas fa-trash" data-song-name="'+songTitle+'"></span>';
            if (songTitle == encodeURIComponent(currentSong.name)) $("#"+songTitle).addClass("currentSong");
        });
    };

    function displayTracks(trackList) {
        $("#tracks-container").empty();
        trackList.forEach(function(track){
            const maxTextLength = Math.max(70-5*trackList.length, 10);
            const displayTitle = "<p class='noselect'>"+shortenText(track.HRname, maxTextLength)+"</p>";
            const volume = parseInt(1000*track.volume);
            const fader = '<span><input type="range" min="0" max="1000" value="'+volume+'" data-orientation="vertical"></input></span>';
            const statusIcons = {auto:"fa-volume-up", manual:"fa-user", mute:"fa-volume-mute"};
            const status = '<a href="javascript:void(0);" class="status-icon fas '+statusIcons[track.status]+'"></a>';
            const trackType = (track.isSample) ? '<br><span class="track-type-icon fas fa-bell"></span>' : '';
            const trackContainer = '<div class="text-center track-container" data-track-name="'+encodeURIComponent(track.name)+'" data-track-HRname="'+track.HRname+'">';
            $("#tracks-container").append(trackContainer + displayTitle + fader + status + trackType + "</div>");
            $('input[type="range"]').rangeslider({ polyfill: false });
            if (track.status != "manual") $("#tracks-container").find(".rangeslider").addClass("rangeslider--disabled");
        });
    };

    // set track slider value from 0~1 value
    function updateSlider(trackName, value) {
        if ($('.track-container[data-track-name="'+encodeURIComponent(trackName)+'"] a').hasClass("fa-user")) return // don't update tracks in manual mode
        const slider = $('.track-container[data-track-name="'+encodeURIComponent(trackName)+'"] input[type="range"]');
        value = parseInt(1000*value)
        if (value != slider.val()) slider.rangeslider().val(value).change();
    };

    function shortenText(text, maxLength){
        return (text.length > maxLength) ? text.substring(0, maxLength-3)+"..." : text;
    };

    // toggle between the play and pause button
    function setPlayPause(isPlaying){
        if (isPlaying) {
            $('.transport-button[data-action="play"]').hide();
            $('.transport-button[data-action="stop"]').show();
        } else {
            $('.transport-button[data-action="stop"]').hide();
            $('.transport-button[data-action="play"]').show();
        }
    }

    // add or refresh the sensor to the sensor's modal
    function addSensorToModal(sensor){
        console.log("adding sensor to modal :", sensor)
        let sensorTemplate = $(".sensor-template").clone();
        sensorTemplate.removeClass("sensor-template").addClass("sensor");
        if (sensor.ledON) sensorTemplate.find(".sensor-led").removeClass("fa-toggle-off").addClass("fa-toggle-on");
        if (sensor.isActive) sensorTemplate.find(".sensor-status").attr("src","../static/imgs/wireless-sensor.png");
        else {
            sensorTemplate.find(".sensor-led").addClass("sensor-disabled");
            sensorTemplate.find(".sensor-calibration").addClass("sensor-disabled");
        }
        sensorTemplate.find(".sensor-name").text(sensor.hostname);
        sensorTemplate.find(".sensor-osc").text(sensor.OSCpath);
        sensorTemplate.find(".sensor-ip").text(sensor.IP);
        sensorTemplate.find(".sensor-count").text(sensor.sensorCount);
        // values and assignations
        for (let i=0; i<sensor.values.length; i++) {
            const value = (sensor.values[i]/10).toString()+"%"; // 682 -> "68%"
            const valueDiv = $("<div>").addClass("sensor-value").attr("data-index", i.toString()).text(value);
            sensorTemplate.find(".sensor-values").append(valueDiv);
            sensorTemplate.find(".sensor-assignations").append($("<div>").addClass("sensor-assignation").attr("data-index", i.toString()));
        };
        $('.sensor[name="'+encodeURIComponent(sensor.hostname)+'"]').remove();
        sensorTemplate.attr("name", encodeURIComponent(sensor.hostname))
        $("#sensor-container").append(sensorTemplate);
        sensorTemplate.show();
    }

     // populates the track-assignation modal with known sensors
     function updateTrackAssignationModal(sensors){
        $("#assign-sensors-container").find(".assign-sensor").remove();
        if (sensors.length < 1) return;
        sensors.forEach(function(sensor){
            const sensorDiv = $("#assign-sensor-template").clone().removeAttr("id").removeClass("template").addClass("assign-sensor")
            sensorDiv.find(".assign-sensor-name").text(sensor.hostname);
            for (let i=0; i<sensor.values.length; i++) {
                const sensorButton=$('<a href="javascript:void(0);" class="sensor-input-assign fas fa-lightbulb">');
                const sensorName =  encodeURIComponent(sensor.hostname+i.toString());
                sensorButton.attr("name", sensorName);
                sensorDiv.find(".assign-sensors-inputs").append(sensorButton);
                sensorDiv.show()
            };
            $("#assign-sensors-container").append(sensorDiv);
        });
    }

});