#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  
#  Copyright 2020 Reso-nance Numérique <laurent@reso-nance.org>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  

import os, glob, subprocess, re, shutil, json
import UI, pd

songsFolder = "static/audio"
songs = {}
currentSong = None
pdSendPort = 50_000
sampleThreeshold = 0.05
pdThreeshold = 0.05
masterVolume = 1.

class Song :

    def __init__(self, folder, reset=False):
        self.folder = folder
        self.name = os.path.basename(folder)
        self.filename = os.path.join(self.folder, self.name + ".json")
        # os.remove(self.filename) # FIXME : debug
        if not self.loadFromFile() :
            self.HRname = self.name
            wavFiles = [f for f in glob.glob(folder+"/*") if f.upper().endswith(".WAV")]
            wavFiles.sort()
            self.tracks = []
            for i in range(len(wavFiles)) : self.tracks.append(Track(wavFiles[i]))
            self.setImage()
            self.generateTracksID() # will also save to file
        self.isPlaying = False
        self.isLoaded = False

    def load(self):
        global currentSong, songs
        print ("loading song", self.name)
        currentSong = self
        self.isLoaded = True
        for song in songs.values() :
            if song is not self : song.isLoaded = False
        pd.clearTracks()
        [track.load() for track in self.tracks]
        # hideous workaround : since the last dynamically created patch in pd never works, we create a dummy additionnal track
        pd.createTrack("dummyTrack", "/dev/null")
        UI.refreshCurrentSong()
        tracksCount = len([1 for t in self.tracks if not t.isSample])
        sampleCount = len(self.tracks)-tracksCount
        UI.toast(f"{tracksCount} pistes et {sampleCount} samples", f"morceau {self.HRname} chargé :", "info")

    def play(self): 
        self.isPlaying = True
        [track.play() for track in self.tracks if not track.isSample]

    def stop(self):
        self.isPlaying = False
        [track.stop() for track in self.tracks]

    def generateTracksID(self):
        """ (re)generate unique tracks ids when the track count is modified"""
        for i in range(len(self.tracks)) :
            self.tracks[i].id = self.HRname.replace(" ","") + str(i)
        self.saveToFile()

    def deleteTrack(self, trackName):
        """ delete the track, the wav file and save to file"""
        track = next(t for t in self.tracks if t.name == trackName)
        os.remove(track.name)
        self.tracks.remove(track)
        print(f"deleted track {trackName} from song {self.name}")
        UI.toast(f"la piste {trackName} à bien été supprimée", "piste supprimée", "info")
        self.generateTracksID() # regen the tracks id to avoid overlapping and saves to file

    def addTrack(self, filename):
        self.tracks.append(Track(filename))
        UI.toast(f"la piste {filename} à bien été ajoutée", "piste ajoutée", "info")
        self.generateTracksID()

    def moveTrack(self, trackName, direction):
        """ change the tracks list order by moving a specific track up or down. No round-robin"""
        track = next(t for t in self.tracks if t.name == trackName)
        currentIndex = self.tracks.index(track)
        self.tracks.remove(track)
        if direction == "down" : currentIndex += 1
        elif direction == "up" and currentIndex >0 : currentIndex -= 1
        self.tracks.insert(currentIndex, track)
        print(f"moved track {trackName} {direction} in song {self.name}")
        self.generateTracksID()

    def toDict(self):
        """ returns a JSON-serialisable dict containing every attribute of this class """
        attributes = {k:v for k,v in self.__dict__.items() if not k.startswith("_")}
        attributes["tracks"] = [t.toDict() for t in self.tracks]
        return attributes
    
    def setImage(self, imagePath=None):
        """ if no path is provided, will look for a file named "thumb.xx" in the song folder. If there isn't any,
        will rename the first image found as "thumb.xx" and delete the others. If no image is found, will set self.image to False"""
        if not imagePath : # look for files named thumb.jpg or thumb.png
            thumbnails = glob.glob(self.folder+"/thumb.*")
            if len(thumbnails) > 0 : self.image = thumbnails[0]
            else : # look for any image file
                imagesFiles = [f for f in glob.glob(self.folder+"/*") if f.upper().endswith((".JPG", ".JPEG", ".PNG", ".GIF"))]
                if len(imagesFiles)>0 : self.setImage(imagesFiles[0]) # recursion, baby !
                else : self.image = False # no image then
        else : 
            if not os.path.isfile(imagePath) : raise FileNotFoundError(f"the path to the song thumbnail {imagePath} doesn't exists")
            else :
                [os.remove(f) for f in glob.glob(self.folder+"/thumb.*") if f!=imagePath] # remove the old thumbnail
                extension = os.path.splitext(os.path.basename(imagePath))[1] # could be .gif, .jpg...
                newPath = self.folder + "/thumb" + extension
                shutil.move(imagePath, newPath) # move to thumb.xx
                self.image = newPath

    def rename(self, newName):
        self.HRname = newName
        self.saveToFile()
                
    def saveToFile(self):
        """ will dump all of it's attributes to a JSON file in the song's folder """
        with open(self.filename, "+w") as f :
            json.dump(self.toDict(), f,  ensure_ascii=False, indent=4)
        print(f"song {self.name} written to {self.filename}")

    def loadFromFile(self):
        """ will restore all of the attributes read from the JSON file and return False if something went wrong"""
        if not os.path.isfile(self.filename) : return False
        try :
            with open (self.filename, "rt") as f : data = json.load(f)
            tracks = data.pop("tracks") # remove the tracks from data to instanciate them separately
            for attr in data : setattr(self, attr, data[attr])
            self.tracks = [Track(t["name"], t["id"], HRname=t["HRname"], isSample=t["isSample"], assignedTo=t["assignedTo"]) for t in tracks]
            notFounds = [t.name for t in self.tracks if not os.path.isfile(t.name)]
            if notFounds :
                print("ERROR : file(s)", notFounds, "not found")
                UI.toast(f"les fichier {notFounds} sont introuvable", f"erreur au chargement de {self.HRname}", "warning")
                return False
            else : print(f"successfully restored song {self.name} from {self.filename}")
            return True
        except json.JSONDecodeError : # corrupted file ?
            print(f"cannot restore song {self.name}, deleting corrupted file {self.filename}")
            UI.toast(f"la configuration de {self.name} à été perdue", f"erreur au chargement de {self.HRname}", "warning")
            os.remove(self.filename)
            return False


class Track:
    def __init__(self, filename, trackID=None, volume=1, HRname=None, isSample=None, assignedTo=None):
        self.name = filename # the path of the file being unique, it is used as an identifier
        self.HRname = self.getHRname() if HRname is None else HRname
        self.isSample = "SAMPLE" in filename.upper() if isSample is None else isSample
        self.volume = volume
        self.status = "auto" # can be "manual, mute or auto"
        self.id = trackID # track name within pd, must not contain spaces
        self.assignedTo = assignedTo # will be set to the hostname of the device
        self.defaultAssignement = None # will be set if the preferred assignement is not yet online 

    def getHRname(self):
        """ returns a human readable track name based on the filename without extension"""
        HRname = os.path.splitext(os.path.basename(self.name))[0]
        HRname = re.sub("sample", "", HRname, flags=re.IGNORECASE)
        return HRname

    def setVolume(self, volume):
        """ manages volume changes differently for sample tracks and loop tracks"""
        if abs(self.volume - volume) > pdThreeshold :
            if self.isSample :
                if volume > sampleThreeshold and self.volume < sampleThreeshold :
                    pd.play(self.id) # stutters samples
                elif volume < sampleThreeshold : pd.stop(self.id)
            self.volume = volume
            pd.setVolume(self.id, self.volume)

    def rename(self, newName):
        """ sets the HRname and save the song to file"""
        self.HRname = newName
        [s.saveToFile() for s in songs.values() if self in s.tracks] # update songs using this track

    def setIsSample(self, isSample):
        """ sets the isSample flag, send it to pd and save the song to file"""
        if self.isSample == isSample : return
        else : 
            self.isSample = isSample
            if currentSong and self in currentSong.tracks : pd.setLoop(self.id, self.isSample)
            [s.saveToFile() for s in songs if self in s.tracks] # update songs using this track

    def setStatus(self, status):
        if status not in ("manual", "auto", "mute") : return
        self.status = status
        if status == "mute" : self.setVolume(0)

    def play(self):
        pd.play(self.id)

    def stop(self):
        pd.stop(self.id)

    def load(self):
        pd.createTrack(self.id, self.name)
        pd.setLoop(self.id, self.isSample)
        pd.setVolume(self.id, self.volume)

    def reload(self):
        pd.openFile(self.id, self.name)

    def toDict(self):
        return {k:v for k,v in self.__dict__.items() if not k.startswith("_")}


def refreshAvailableSongs():
    """(re)create song objects from files in the songs directory and add them to the song dict if not already there, returns said dict"""
    global songs
    print("refreshing available songs")
    songsFolders = [p for p in os.listdir(songsFolder) if os.path.isdir(songsFolder+"/"+p)]
    songsFolders.sort()
    for d in songsFolders:
        song = Song(songsFolder +"/"+ d)
        if not song.name in songs : 
            songs[song.name] = song
            print("\n  added song", song.name, ":")
            for t in song.tracks : print("    ","sample" if t.isSample else "track", t.name)
        else : print ("  song", song.name, "already in songs")
    return songs

def getSongList():
    """ returns a sorted list of the songs names listed in global songs"""
    refreshAvailableSongs()
    songlist = list(songs.keys())
    songlist.sort()
    return songlist

def importSongFromZip(zipPath):
    """ unzip the file to a folder, pruning the directory structure. Removes all unsupported files.
    If no wav files are found, returns False and remove the zip and folder.
    Else, move the newly created folder to the songs dir and returns the new song's name."""
    print("importing song from", zipPath)
    zipName = os.path.basename(zipPath)
    zipNameWithoutExt = os.path.splitext(zipName)[0] # will be the folder name
    subprocess.Popen(f"cd {UI.tempFolder} && unzip -oj {zipName} -d {zipNameWithoutExt}", shell=True).wait()
    zipFolder = os.path.join(UI.tempFolder, zipNameWithoutExt)
    for f in glob.glob(zipFolder+"/*"):
        if os.path.splitext(f)[1].upper() not in (".WAV", ".JPG", ".JPEG", ".PNG", ".GIF", ".JSON") :
            os.remove(f)
            print("  deleted unsupported file",f)
    wavFiles = [f for f in glob.glob(zipFolder+"/*") if f.upper().endswith(".WAV")]
    if not wavFiles :
        print("  ERROR : no .wav files found !")
        UI.toast("aucun fichier .wav trouvé", "importation échouée :", "danger", delay=10)
        os.removedirs(zipFolder)
        os.remove(zipPath)
        return False
    else : 
        if os.path.isdir(os.path.join(songsFolder, zipNameWithoutExt)) : # if folder already exists
            shutil.rmtree(os.path.join(songsFolder, zipNameWithoutExt))# replace it
            UI.toast(f"le morceau {zipNameWithoutExt} sera écrasé", "morceau déjà existant,", "warning", delay=10)
        shutil.move(zipFolder, songsFolder)
        newSong = Song(songsFolder +"/"+ zipNameWithoutExt)
        songs.update({newSong.name : newSong})
        newSong.saveToFile()
        print(f"successfully added song {zipNameWithoutExt} containing {len(wavFiles)} tracks")
        UI.toast(f"le morceau {zipNameWithoutExt} contenant {len(wavFiles)} pistes à bien été ajouté", "importation réussie :", "success")
        # refreshAvailableSongs() # FIXME : songs updates currently broken in songs.js
        return newSong

def setMasterVolume(value):
    global masterVolume
    value = float(value) /1000
    masterVolume = value
    pd.setMasterVolume(value)

def unloadAll():
    if currentSong and currentSong.isPlaying : currentSong.stop()
    pd.clearTracks()
