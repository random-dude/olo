#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  
#  Copyright 2020 Reso-nance Numérique <laurent@reso-nance.org>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
#  
from flask import Flask, render_template, redirect, request, flash, Response
from flask_socketio import SocketIO, emit
import os, logging, subprocess, time, gevent
from urllib.parse import unquote
from threading import Thread

import audioFiles, sensors

tempFolder = "static/tmp"
uploadedFile = None
isDebug = True
refreshUIdelay = .1 # interval in seconds between two UI-updates (tracks volumes and sensors values)
updateTracksSensors = False # set this to False to stop updating sensors values on the UI

if __name__ == '__main__':
    raise SystemExit("this file is made to be imported as a module, not executed")

""" Initialize Flask and flask-socketIO """
app = Flask(__name__) 
app.url_map.strict_slashes = False # don't terminate each url by / ( can mess with href='/#' )
socketio = SocketIO(app, async_mode="gevent")


# --------------- FLASK ROUTES ----------------

@app.route('/')
def rte_homePage():
    return render_template('index.html')

@app.route('/songs', methods=["GET","POST"])
def rte_songsPage():
    """ this route will be used to serve a page or upload files """
    if request.method == 'GET' : # serve the page
        return render_template('songs.html')

    elif request.method == 'POST' and 'imageFile' in request.files: # images used as thumbnails for songs
        songName = unquote(request.form.get("songName"))
        destinationFolder = audioFiles.songs[songName].folder # the pic is stored within the song dir, with the audio files
        file = request.files['imageFile']
        if file.filename :
            if file.filename.upper().endswith((".JPG", ".JPEG", ".GIF", ".PNG")):
                filePath = os.path.join(destinationFolder, file.filename)
                file.save(filePath) # will overwrite if they have the same name
                print("successfully uploaded image", file.filename, "to", destinationFolder)
                toast(f"l'image {file.filename} à bien été importée", "téléversement réussi")
                audioFiles.songs[songName].setImage(filePath)
                return Response("thanks, man"), 200 # the response content itself is never read, only the response code
            else :
                print("uploaded image", file.filename, "doesn't look like an image to me")
                toast(f"le fichier {file.filename} est-il bien au format jpg, gif ou png ?", "erreur", "warning")
                
    elif request.method == "POST" and "wavFile" in request.files:
        songName, trackPath = unquote(request.form.get("songName")), unquote(request.form.get("trackName"))
        file = request.files['wavFile']
        if file.filename :
            if file.filename.upper().endswith(".WAV"):
                if trackPath == "newTrack" : # new track should be saved on the song folder under it's original filename
                    songFolder = os.path.join(audioFiles.songsFolder, songName)
                    if not os.path.isdir(songFolder) : # new song
                        # make sure the song name doesn't contain restricted characters for unix paths
                        for c in ["&","/","\\","#",",", "$","~","%","'",'"',":","*","?","<",">","{", "}","@", ";"] : songName.replace(c, "_")
                        songFolder = os.path.join(audioFiles.songsFolder, songName)
                        os.makedirs(songFolder)
                        audioFiles.songs.update({songName : audioFiles.Song(songFolder)})
                        audioFiles.songs[songName].HRname = "nouveau morceau"
                        audioFiles.songs[songName].saveToFile()
                        socketio.emit("updateSong", audioFiles.songs[songName].toDict(), namespace="/songs")
                    filepath = os.path.join(songFolder, file.filename)
                    file.save(filepath)
                    thisSong = audioFiles.songs[songName]
                    thisSong.addTrack(filepath)
                    socketio.emit("refreshSongEditor", thisSong.toDict(), namespace="/songs")
                    toast(f"la piste {file.filename} à bien été ajoutée", "téléversement réussi")
                else : 
                    file.save(trackPath) # will overwrite the current file (track name)
                    toast(f"la piste {file.filename} à bien été remplaçée", "téléversement réussi")
                return Response("power to the people !"), 200 # the response content itself is ignored, only the response code matters
            else :
                print("uploaded wav file", file.filename, "doesn't look like an wav to me")
                toast(f"le fichier {file.filename} est-il bien un .wav ?", "erreur", "warning")
                
    elif request.method == "POST" and "zipFile" in request.files:
        file = request.files['zipFile']
        if file.filename :
            if file.filename.upper().endswith(".ZIP"):
                filePath = os.path.join(tempFolder, file.filename)
                file.save(filePath) # will overwrite the current file
                Thread(target=processZippedSong, args=(filePath,)).start()
                return Response("one love"), 200 # the response content itself is ignored, only the response code matters
            else :
                print(f"uploaded zip file {file.filename} doesn't look like an zip to me")
                toast(f"le fichier {file.filename} est-il bien un .zip ?", "erreur", "warning")

@app.route('/shutdown')
def rte_bye():
    return render_template('shutdown.html')


# --------------- SOCKET IO EVENTS ----------------
# ------------------- main page -------------------

@socketio.on('connect', namespace='/home')
def onConnect():
    global updateTracksSensors
    print("client connected to /home, session id : "+request.sid)
    refreshCurrentSong()
    socketio.emit("masterVolume", int(audioFiles.masterVolume*1000), namespace="/home")
    refreshSongList()
    refreshSensorsModal()
    if not updateTracksSensors :
        updateTracksSensors = True
        Thread(target=refreshSensorsValues).start() # will update tracks volumes and sensor values
        print("started the refresh UI thread")
    sensorModalUpdateAssignations()

@socketio.on('disconnect', namespace='/home')
def onDisconnect():
    global updateTracksSensors
    print("client disconnected from /home")
    updateTracksSensors = False

@socketio.on('loadSong', namespace='/home')
def loadSong(songName):
    songName = unquote(songName)
    if songName in audioFiles.songs : 
        audioFiles.songs[songName].load()
        gevent.spawn_later(.5, sensors.autoAssign) # give time for the song to load
    sensorsCount = sum([s.sensorCount for s in sensors.knownSensors.values() if s.isActive])
    if sensorsCount < len(audioFiles.currentSong.tracks) :
        toast(f"ce morceau contient plus de pistes qu'il n'y a de capteurs connectés. "
        "Certaines pistes n'ont pas pu être assignées à un capteur", "capteurs manquants ", "danger", 15)
    else : print("ERROR : asked to load non-existing song", songName)

@socketio.on('transportSong', namespace='/home')
def transportSong(action):
    if action == "stop" : audioFiles.currentSong.stop()
    elif action == "play" : audioFiles.currentSong.play()
    print("asked to", action,"song", audioFiles.currentSong.name )

@socketio.on('calibrateAmbient', namespace='/home')
def calibrateAmbient():
    connectedSensors = [s.hostname for s in sensors.knownSensors.values() if s.isActive]
    if connectedSensors : 
        print("calibrating ambient on every known sensor")
        toast(f"la calibration au noir de tous les capteurs connectés ({', '.join(connectedSensors)}) à été effectuée", "calibration nuit ")
        sensors.broadcastOSC("/calibrateAmbiant")
    else : toast("la calibration n'a pu être effectué", "aucun capteur connecté ", "warning",5)

@socketio.on('toggleStatus', namespace='/home')
def toggleStatus(trackname):
    """ toggle track status from "mute" to "manual" to "auto" """
    trackname = unquote(trackname)
    track = [t for t in audioFiles.currentSong.tracks if t.name == trackname]
    if len(track) < 1 : 
        print("ERROR : couldn't find track", trackname, "in current song",audioFiles.currentSong.name)
        return
    track = track[0]
    if track.status == "auto" : track.setStatus("mute") # I vote for a switch/case structure in python 4
    elif track.status == "mute" : track.setStatus("manual")
    elif track.status == "manual" : track.setStatus("auto")
    socketio.emit("changedTrackStatus", {"name" : track.name, "status":track.status}, namespace="/home")

@socketio.on("sliderTouched", namespace="/home")
def sliderTouched(trackName, value):
    """ set the track volume if track status is manual"""
    trackName = unquote(trackName)
    if trackName == "master": audioFiles.setMasterVolume(value)
    else :
        try : track = next(t for t in audioFiles.currentSong.tracks if t.name == trackName)
        except StopIteration : print(f"ERROR : slider moved for track {trackName} : not found") 
        value = float(value) / 1000 # slider goes from 0~1000 where track volume is a float 0~1
        if track.status == "manual" : track.setVolume(value)

@socketio.on("playSample", namespace="/home")
def playSample(trackName):
    track = next(t for t in audioFiles.currentSong.tracks if t.name == unquote(trackName))
    track.play()

@socketio.on("masterStatus", namespace="/home")
def masterStatus(status):
    """ set the same status to all tracks on the currently loaded song"""
    for track in audioFiles.currentSong.tracks: 
        track.setStatus(status)
    print("set master status to", status)

@socketio.on("sensorLed", namespace="/home")
def sensorLed(hostname, ledValue):
    thisSensor = sensors.knownSensors[unquote(hostname)]
    if thisSensor.isActive :
        thisSensor.toggleLED()
        socketio.emit("updateSensorModal", [thisSensor.toDict()], namespace="/home")

@socketio.on("sensorCalibMax", namespace="/home")
def sensorCalibMax(hostname):
    thisSensor = sensors.knownSensors[unquote(hostname)]
    if thisSensor.isActive :
        thisSensor.calibrateMax()
        gevent.spawn_later(1, toast, f"la lumière maximum pour tous les capteurs de la carte {hostname} est calibrée")
        print("calibrating max light for sensor", hostname)

@socketio.on("sensorAssign", namespace="/home")
def sensorAssign(sensorName, trackName): 
    sensorHostname = unquote(sensorName)[:-1]
    sensorValueIndex = int(unquote(sensorName)[-1])
    trackName = unquote(trackName)
    try : 
        track = next(t for t in audioFiles.currentSong.tracks if t.name == trackName)
        # FIXME : incorporate this into a proper Track method ?
        if track.assignedTo and track.assignedTo[:-1] in sensors.knownSensors and track.assignedTo[:-1] != sensorHostname:
        # this track is already assigned to another connected sensor
            prevAssignedSensorHostname = track.assignedTo[:-1]
            prevAssignedSensorIndex = int(track.assignedTo[-1])
            sensors.knownSensors[prevAssignedSensorHostname].assignedTracks[prevAssignedSensorIndex] = None # free the previous sensor
            assignSensorValueToTrack(sensorHostname, sensorValueIndex, trackName) # assign the track to the requested sensor
            toast(f"la piste {track.HRname} n'est plus assignée au capteur {prevAssignedSensorHostname}-#{prevAssignedSensorIndex} "
            "mais à {sensorHostname}-#{sensorValueIndex}", "assignation modifiée", "info")
            # sensors.findAssignation(sensors.knownSensors[prevAssignedSensorHostname]) # find another assignation to the previous sensor
        else :
            assignSensorValueToTrack(sensorHostname, sensorValueIndex, trackName)
            # toast(f"la piste {track.HRname} à été assignée au capteur n°{sensorValueIndex+1} de la carte {sensorHostname}", "nouvelle assignation ", "info")
        prevAssignedTracks = [t for t in audioFiles.currentSong.tracks if t.assignedTo == unquote(sensorName) and t.name != track.name]
        if prevAssignedTracks : # one sensor will only be assigned to one track
            for t in prevAssignedTracks : t.assignedTo = None
            audioFiles.currentSong.saveToFile()
        sensors.autoAssign()

    except StopIteration : toast("essayez de recharger le morceau", "impossible d'assigner le capteur", "warning")

@socketio.on('sensorLearn', namespace='/home')
def startLearning(trackName):
    Thread(target=sensors.learnSensor, args=(trackName,)).start()

@socketio.on('powerOFF', namespace='/home')
def powerOFF():
    print("shutdown from UI")
    gevent.sleep(2)# allow time for the shutdown page to show
    os.system("sleep 2 && sudo shutdown now&") # allow exitCleanly the time to run
    raise SystemExit


# --------------- SOCKET IO EVENTS ----------------
# ------------------ songs page -------------------

@socketio.on('connect', namespace='/songs')
def onConnectSongs():
    global updateTracksSensors
    updateTracksSensors = False
    print("client connected to /songs, session id : "+request.sid)
    refreshSongsInfos()
    audioFiles.unloadAll()

@socketio.on('disconnect', namespace='/songs')
def onDisconnectSongs():
    print("client disconnected from /songs")

@socketio.on('updateSongsInfos', namespace='/songs')
def updateSongsInfos():
    refreshSongsInfos()

@socketio.on('downloadSong', namespace='/songs')
def downloadSong(songName):
    """ zip a song folder on a separate thread """
    path = audioFiles.songs[unquote(songName)].folder
    Thread(target=zipFolder, args=(path,)).start()

@socketio.on('deleteSong', namespace='/songs')
def deleteSong(songName):
    songName = unquote(songName)
    path = audioFiles.songs[songName].folder
    print("deleting song %s (folder : %s)" %(songName, path))
    if audioFiles.songs[songName].isLoaded : # load another song if this one is in use
        otherSongs = [s for s in audioFiles.songs if s != songName]
        if len(otherSongs) > 1 : audioFiles.songs[otherSongs[0]].load()
        else : raise SystemError("deleted the only song available")# FIXME : handle this ? will it ever happen ?
    subprocess.Popen("rm -rf "+path.replace(" ", "\ "), shell=True) # definitely no security breach there, move along folks
    toast(f"il ne pourra pas être restauré", f"le morceau {audioFiles.songs[songName].HRname} à été supprimé")
    del audioFiles.songs[songName]
    # FIXME : update song list

@socketio.on('songNameChanged', namespace='/songs')
def renameSong(songName, newName):
    songName, newName = unquote(songName), unquote(newName)
    if songName in audioFiles.songs :
        audioFiles.songs[songName].rename(newName)
        print("renamed song", songName, "to", newName)
        socketio.emit("updateSong", audioFiles.songs[songName].toDict(), namespace="/songs")
        
@socketio.on('deleteTrack', namespace='/songs')
def deleteTrack(songName, trackname):
    thisSong = audioFiles.songs[unquote(songName)]
    thisSong.deleteTrack(unquote(trackname))
    socketio.emit("refreshSongEditor", thisSong.toDict(), namespace="/songs")

@socketio.on('moveTrack', namespace='/songs')
def moveTrack(songName, trackname, direction):
    thisSong = audioFiles.songs[unquote(songName)]
    thisSong.moveTrack(unquote(trackname), direction)
    socketio.emit("refreshSongEditor", thisSong.toDict(), namespace="/songs")

@socketio.on("trackNameChanged", namespace="/songs")
def renameTrack(songName, oldTrackName, newTrackName):
    oldTrackName, newTrackName, songName = unquote(oldTrackName), unquote(newTrackName), unquote(songName)
    print(f"renaming track {oldTrackName} to {newTrackName} for song {songName}")
    song = audioFiles.songs[songName]
    track = next(t for t in song.tracks if t.name == oldTrackName)
    track.rename(newTrackName)
    socketio.emit("updateSong", song.toDict(), namespace="/songs")

@socketio.on("isSampleChanged", namespace="/songs")
def setIsSample(songName, trackName, isSample):
    songName, trackName = unquote(songName), unquote(trackName)
    if songName not in audioFiles.songs : return
    track = next(t for t in audioFiles.songs[songName].tracks if t.name == trackName)
    track.setIsSample(isSample)

# --------------- FUNCTIONS ----------------
 
def refreshCurrentSong():
    currentSong = audioFiles.currentSong.toDict() if audioFiles.currentSong else None
    socketio.emit("currentSong", currentSong, namespace="/home")


def refreshSongList() : 
    socketio.emit("songList",audioFiles.getSongList(), namespace="/home")

def refreshSensorsModal():
    socketio.emit("refreshSensorsModal", sensors.getKnownSensors(), namespace="/home")

def refreshSensorsValues():
    while updateTracksSensors:
        sensorsValues, trackVolumes = {}, {}
        sensorsValues = {s.hostname:s.values for s in sensors.knownSensors.values() if s.isActive}
        if audioFiles.currentSong : trackVolumes = {t.name:t.volume for t in audioFiles.currentSong.tracks}
        if sensorsValues or trackVolumes :
            socketio.emit("updateUI", {"sensors":sensorsValues, "tracks":trackVolumes}, namespace="/home")
        gevent.sleep(refreshUIdelay)
    print("stopped the refresh UI thread")

def refreshSongsInfos():
    infos=[audioFiles.songs[s].toDict() for s in audioFiles.getSongList()]
    socketio.emit("songsInfos", infos, namespace="/songs")

def zipFolder(path):
    """ blocking function which zip a folder then sends a headsup when finished """
    outputName = tempFolder + "/" + os.path.basename(path) + ".zip"
    subprocess.Popen(f"zip -jr {outputName} {path}/*", shell=True).wait()
    socketio.emit("downloadReady", "../"+outputName, namespace="/songs")
    toast(f"s'il ne se lance pas, vérifier que les popups ne sont pas désactivés pour ce site", "le téléchargement est prêt")

def processZippedSong(zipPath):
    """ blocking function which process the zip and informs the UI
    if everything went well"""
    newSong = audioFiles.importSongFromZip(zipPath)
    if newSong :
        socketio.emit("zipUploadSuccess", newSong.toDict(), namespace="/songs" )

def toast(text, title=None, style=None, delay=None):
    """ will display a message to the user regardless of the current page
    :param text: this string will be displayed to the user as is
    :param title: this string will be displayed before the text in bold
    :param style: "primary", "secondary", "success", "danger", "warning", "info"... name your bootstrap colour
    :param delay: time in seconds after which the toast will be automatically dismissed
    """
    toastParameters = {"text":text, "title":title, "style":style}
    toastParameters.update({"delay": None if delay==None else int(delay*1000)})
    socketio.emit("toast", toastParameters, namespace="/notifications")

def assignSensorValueToTrack(sensorHostname, valueIndex, trackName):
    """ force assign a sensor value to a track in current song and save to file """
    if sensorHostname in sensors.knownSensors and trackName in [t.name for t in audioFiles.currentSong.tracks] :
        sensors.knownSensors[sensorHostname].assignedTracks[valueIndex] = trackName
        track = next(t for t in audioFiles.currentSong.tracks if t.name == trackName)
        track.assignedTo = sensorHostname+str(valueIndex)
        audioFiles.currentSong.saveToFile()

def sensorModalUpdateAssignations():
    """ sends the HRname of every track assigned to this sensor to update the sensors modal"""
    assignations=[]
    for hostname in sensors.knownSensors:
        trackNames = []
        for trackName in sensors.knownSensors[hostname].assignedTracks :
            if trackName and trackName in [t.name for t in audioFiles.currentSong.tracks] :
                trackNames.append(next(t.HRname for t in audioFiles.currentSong.tracks if t.name == trackName))
            else : trackNames.append("libre")
        assignations.append({"hostname":hostname, "assignations":trackNames})
    socketio.emit("updateSensorAssignation", assignations ,namespace="/home")
