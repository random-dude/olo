#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  
#  Copyright 2020 Reso-nance Numérique <laurent@reso-nance.org>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
from gevent import subprocess
import os, threading

udpPort = 50_000
patchPath = os.path.abspath("pd/main.pd")
pdPath = None # should be None if pd is in bash $PATH or the absolute path to it's bin folder
pdOptions = ["-r", "44100", "-alsa","-nogui", "-open", f"'{patchPath}'"]
process = None

def start():
    """ launch puredata using the pd options defined above"""
    global process
    if not process :
        pdExecutable = pdPath+"pd " if pdPath else "pd "
        cmd = pdExecutable + " ".join(pdOptions)
        process = subprocess.Popen(cmd, shell=True)

def openFile(trackName, path):
    """ sends the path to the audio file to pd for a given track """
    path=(os.path.abspath(path))
    send(trackName, "open", path)

def play(trackName) :
    """ sends the play command to pd for a given track """
    send(trackName, "play")

def stop(trackName):
    """ sends the play command to pd for a given track """
    send(trackName, "stop")

def setLoop(trackName, isSample):
    """ set the loop parameter in pd for a given track """
    send(trackName, "loop", 0. if isSample else 1.)

def setVolume(trackName, volume):
    """ set the volume parameter in pd for a given track 
    :param trackName: the track ID in pd's tracks container
    :param volume: float 0=mute, 1= full volume"""
    volume=max(0, min(1, volume)) # constrain 0~1
    send(trackName, "volume", volume)

def createTrack(trackName, path):
    """ sends the add command to pd to dynamically generate a track """
    path=os.path.abspath(path) # get the full absolute path
    send("tracks", "add", trackName)
    openFile(trackName, path)

def clearTracks():
    """ sends the clear command to pd which empties the track container """
    print("cleared all tracks in pd container")
    send("tracks", "clear")

def setMasterVolume(volume):
    """ sends the master volume command to pd"""
    volume=max(0, min(1, volume)) # constrain 0~1
    send("master", volume)

def send(*args):
    """ sends every arg as UDP packets to pd
    :param args: can be anything (float, str...), spaces must be escaped"""
    pdsend = pdPath+"pdsend" if pdPath else "pdsend"
    args = " ".join([str(a).replace(" ", "\ ") for a in args])
    os.system(f"echo '{args}' | {pdsend} {udpPort} localhost udp") # Popen won't work here since it's called from child threads (OSC callbacks)