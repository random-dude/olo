#!/usr/bin/env python
# -*- coding: utf-8 -*-
#  
#  Copyright 2020 Reso-nance Numérique <laurent@reso-nance.org>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#  
from gevent import monkey; monkey.patch_all()
import atexit, signal, os

import UI, audioFiles, sensors

flaskBind = "127.0.0.1" if __debug__ else "10.0.0.1" # FIXME DEBUG
HTTPlisteningPort = 8080


def exitCleanly(*args):
    """ closes every thread, kills puredata, clean temp folder
    param : args are not used but provided when called from SIGTERM"""
    print("exiting timeout thread")
    sensors.runTimeoutThread = False
    print("exiting OSCserver thread")
    sensors.listenToOSC = False
    print("terminating puredata and gunicorn...")
    os.system('killall -KILL pd gunicorn:\ worker gunicorn:\ master')
    print("cleaning temporary folder")
    os.system("rm -f " + UI.tempFolder + "/*")
    raise SystemExit


if __name__ == '__main__':
    if __debug__ : 
        print("starting in DEBUG mode")
        audioFiles.pd.pdPath = "/home/lascarKapac/Programmes/pd-0.50-2/bin/"
    signal.signal(signal.SIGTERM, exitCleanly) # register this exitCleanly function to be called on sigterm
    atexit.register(exitCleanly) # idem when called from within this script
    if not os.path.isdir(UI.tempFolder) : os.mkdir(UI.tempFolder)
    print("starting the timeout thread...")
    sensors.startTimeoutThread()
    print("killing previous instances of pd or gunicorn...")
    os.system('killall -KILL pd gunicorn:\ worker gunicorn:\ master')
    print("starting puredata...")
    audioFiles.pd.start()
    print("refreshing song list...")
    audioFiles.refreshAvailableSongs()
    defaultSong = audioFiles.getSongList() [0]
    if len(audioFiles.songs) == 0 : raise SystemError("ERROR : no songs found in", audioFiles.songsFolder)
    print("starting the OSC server thread...")
    sensors.listen()
    try: 
        print(f"starting up webserver on {flaskBind}:{HTTPlisteningPort}...")
        flaskOptions = {"host":flaskBind, "port":HTTPlisteningPort}
        if __debug__ : flaskOptions.update({"debug":True, "use_reloader":False})
        UI.socketio.run(UI.app, **flaskOptions)  # Start the asynchronous web server (flask-socketIO)
        # _=os.system(f'gunicorn -k "geventwebsocket.gunicorn.workers.GeventWebSocketWorker"  -w 1 -b {flaskBind}:{HTTPlisteningPort} UI:app')

    except KeyboardInterrupt : raise SystemExit # this will call exitCleanly()
