/*--------------This code is for a Wemos D1 board equipped with a MCP3204 (12bits 8 channels ADC).
 * WARNING : this code is a stripped down version of a much bigger project interconnecting a variety of differents sensors and effectors inside a complete ecosystem.
 *           It has been adapted to another project in a pinch and is provided as-is, without much documentation.
 * The ADC uses SPI to communicate with the D1, however the MCP320X library used here do not rely on a fully fledged SPI library but simulate SPI by manual bit banging

          Wemos D1          MCP3208
            3V3 ------------- vdd
            3V3 ------------- vRef
            Gnd ------------- aGnd
            Gnd ------------- dGnd
            D5 -------------- CS/EN
            D6 -------------- dIn
            D7 -------------- dOut
            D8 -------------- CLK

compiled with ESP8266 v2.4.1 and OSC 1.3.3
*/
#include "Arduino.h"
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>
#include <OSCMessage.h>
#include <EEPROM.h>
#include "MCP320X.h"

#define OSCADDRESS "/C"
#define MINRATE 1000// minimum delay between two status messages to avoid being detected as offline (in ms)
#define MAXRATE 50// average delay between two status messages (in ms). With 4 ESP8266 connected to a rPi, hell might break loose under 20ms 
#define OTA_TIMEOUT 5 // time in seconds after which the device resume it's normal activity if no OTA firmware is comming 
#define MEANCALIBRATION 100 // number of measures to average during calibration
#define DEADBAND 50 // below this value(0~1000), the sensor value will be set to 0 to filter the low-end
//#define SERIAL_DEBUG
#define WIFIMAXLENGTH 64 // maximum SSID and PSK length (in char)
#define MAXWIFIATTEMPTS 120 // number of tries before reverting to default ssid/pss. There is ~2 seconds of delay inbetween attempts
#define ONBOARD_LED 2

const String MACaddress=WiFi.macAddress().substring(9); // remove the manufacturer ID (first 9 characters) from the MAC
String hostname="LDR_"+MACaddress;
static char* defaultPSK = "havefunordietrying";
static char* defaultSSID = "LaurentOutan";
const int listenPort = 8000;
int targetPort = 9000;
unsigned int wifiAttempts = 1;
IPAddress serverIP=IPAddress({10,0,0,1}); // communication with the Pi server
IPAddress IPfromLastMessageReceived;
WiFiUDP udpserver;
char incomingPacket[255];
char incomingAddress[128];
long loopTimer = 0;
long keepAliveTimer = 0;
const unsigned int eepromTag= 22583; // random int used to check if eeprom has been written before. Change this to clear EEPROM on next boot
bool OTA_asked = false; // flag which become true for OTA_TIMEOUT seconds after receiving a /beginOTA message to suspend device activity while flashing
#ifdef OSCDEBUG
long debugLoopTimer = 0;
#endif

#ifdef SERIAL_DEBUG
  #define debugPrint(x)  Serial.print (x)
  #define debugPrintln(x)  Serial.println (x)
#else
  #define debugPrint(x)
  #define debugPrintln(x)
#endif

//---------------- Hardware-specific variables ------------------
static const int inputsCount = 3;
int currentReading[inputsCount];
int lastReading[inputsCount];
int currentValue[inputsCount];
int lastValueSent[inputsCount];
long sum[inputsCount];
int meanCounter[inputsCount];
int defaultCalibMin = 0; int defaultCalibMax = 4095; // 12 bits
struct {int calibMin[inputsCount]; int calibMax[inputsCount]; unsigned int filter = 0; unsigned int tag = eepromTag; char PSK[WIFIMAXLENGTH]; char mySSID[WIFIMAXLENGTH];} parameters;
MCP320X mcp3204 = MCP320X(8, D8, D6, D7, D5); // MCP320X(MCP3204, CLOCK_PIN, MOSI_PIN, MISO_PIN, CS_PIN)
//--------------------------------------------------------------

void setup() {
  hostname.replace(":", ""); // remove the : from the MAC address
  char hostnameAsChar[hostname.length()+1];
  hostname.toCharArray(hostnameAsChar, hostname.length()+1);
  #ifdef SERIAL_DEBUG
  Serial.begin(115200);
  #endif
  EEPROM.begin(sizeof(parameters));
  loadCalibFromEeprom();
  delay(10);
  connectToWifi(hostnameAsChar, parameters.mySSID, parameters.PSK);
  udpserver.begin(listenPort); // start listening to specified port
  loopTimer = millis();
  debugPrint("hostname : ");debugPrintln(hostname);
  sendID();
 
//---------------- Hardware-specific setup ------------------

  for (int i=0; i<inputsCount; i++) {currentReading[i] = mcp3204.readADC(i);}
  pinMode(ONBOARD_LED, OUTPUT);
  digitalWrite(ONBOARD_LED, LOW); // on the D1 mini, the led pin is sinking instead of sourcing so the led is on when GPIO2 is pulled low

//--------------------------------------------------------------

}

void loop() {
  if (OTA_asked) { // when receiving a /beginOTA message, this loop will wait for OTA to begin instead of carrying on the main loop
    for (int i=0; i<OTA_TIMEOUT; i++) {
      ESP.wdtFeed();
      yield();
      ArduinoOTA.handle();
      delay(1000);
    }
    OTA_asked = false;
  }
  else {

//---------------- Hardware-specific mainloop ------------------  
  
    for (int i=0; i<inputsCount; i++) {
      lastReading[i]=currentReading[i];
      currentReading[i]=mcp3204.readADC(i);
      sum[i] += currentReading[i];
      meanCounter[i]++;
    }
        
    if (millis() > loopTimer + MAXRATE && meanCounter > 0) { // time to send data : calculate the average
      for (int i=0; i<inputsCount; i++) {
        currentValue[i] = sum[i] / meanCounter[i];
        int constrainedCurrentValue = constrainValue(currentValue[i],i);// constrain between 0 and 1000 using the calibration
        if (constrainedCurrentValue != lastValueSent[i]) { // avoid sending unnecessary updates
          lastValueSent[i] = constrainedCurrentValue;
          sendStatus();
          loopTimer = millis();
        }
       
      else if( millis() > loopTimer + MINRATE) {
        debugPrintln("Keep alive ");
        sendStatus();
        loopTimer = millis();
      }
      
      meanCounter[i]=0;
      sum[i]=0;
     }
    }
     delay(2);// needed to be able to receive OSC messages
     
  //--------------------------------------------------------------
  
  // Read OSC messages sent to this adress (or broadcasted)
    OSCMessage* msg = getOscMessage();
    if (msg != NULL) {
      if ( msg->fullMatch("/beginOTA") ) {
        debugPrintln("Asked to prepare for OTA flashing");
        OTA_asked = true;
      } else if ( msg->fullMatch("/credentials") && msg->isString(0) && msg->isString(1) ) {
        debugPrintln("changing credentials :");
        char SSIDbuffer[WIFIMAXLENGTH];char PSKbuffer[WIFIMAXLENGTH];
        msg->getString(0,SSIDbuffer,WIFIMAXLENGTH);
        msg->getString(1,PSKbuffer,WIFIMAXLENGTH);
        memcpy(parameters.PSK, PSKbuffer, sizeof(PSKbuffer[0])*WIFIMAXLENGTH);
        memcpy(parameters.mySSID, SSIDbuffer, sizeof(SSIDbuffer[0])*WIFIMAXLENGTH);
        debugPrint("new SSID=");debugPrint(parameters.mySSID);debugPrint(", new PSK=");debugPrintln(parameters.PSK);
        writeCalibToEeprom();
        delay(300);
        ESP.restart();
      } else if  ( msg->fullMatch("/calibrateAmbiant") ) {
        for (int i=0; i<inputsCount; i++) {
          const int calibMin = calibrate(i);
          if (calibMin != parameters.calibMax[i]) {// calibMin==calibMax will lead to a division by zero later in the map function, see https://github.com/esp8266/Arduino/issues/2219
            parameters.calibMin[i] = calibMax;
            debugPrintln("calibration min set to: "+String(parameters.calibMin[i])+" on channel #"+i);
          }
          else debugPrintln("calibration min FAILED for channel #"+i);
          }
        writeCalibToEeprom();
      } else if  ( msg->fullMatch("/calibrateMax") ) {
        for (int i=0; i<inputsCount; i++) {
          const int calibMax = calibrate(i);
          if (calibMax != parameters.calibMin[i]) parameters.calibMax[i] = calibMax;
          else parameters.calibMax[i]=0; // global calibration will ignore this channel
          }
        int maxCalib = 0; // find the maximum calibration which will be shared accross all sensors, to avoid having to light each one separately
        for (int i=0; i<inputsCount; i++) {maxCalib = max(maxCalib, parameters.calibMax[i]);}
        for (int i=0; i<inputsCount; i++) {
          if (maxCalib != parameters.calibMin) {
            parameters.calibMax[i] = maxCalib;
            debugPrintln("calibration max set to: "+String(parameters.calibMax[i])+" on channel #"+i);
          }
          else debugPrintln("calibration max FAILED for channel #"+i);
          }
        writeCalibToEeprom();
      } else if  ( msg->fullMatch("/sendID") ) {
        sendID();
      } else if  ( msg->fullMatch("/ledON") ) {
        digitalWrite(ONBOARD_LED, LOW);
      } else if  ( msg->fullMatch("/ledOFF") ) {
        digitalWrite(ONBOARD_LED, HIGH);
      }
      delete msg;
    }
  
    ESP.wdtFeed(); // avoid triggering the ESP watchdog
    yield(); // same but different
  }
}

//---------------- Hardware-specific functions ------------------  

void sendStatus(){
  debugPrintln("Sent OSC status :");
  for (int i=0; i< inputsCount; i++) {
    debugPrint("ch#"+String(i)+"raw data = ");debugPrint(currentReading[i]);
    debugPrint(" averaging ");debugPrint(meanCounter[i]);
    debugPrint(" reading(s), value = ");debugPrintln(currentValue[i]);
  }
  OSCMessage* msg = new OSCMessage(OSCADDRESS);
  for (int i=0; i< inputsCount; i++) {msg->add((int) lastValueSent[i]);}
  sendOsc(msg, serverIP, targetPort);
  delete(msg);
}

// will send (/OSCADDRESS_ID hostname IPaddress inputsCount)
void sendID(){
  debugPrintln("Sent OSC ID :");
  debugPrint("IP : "); debugPrint(WiFi.localIP()); debugPrint(", hostname : "+hostname);debugPrintln(", sensor count : "+String(inputsCount));
  char hostnameAsChar[hostname.length()+1];
  hostname.toCharArray(hostnameAsChar, hostname.length()+1);
  OSCMessage* msg = new OSCMessage("/myID");
  msg->add(OSCADDRESS);
  msg->add(hostnameAsChar);
  msg->add( (char*) WiFi.localIP().toString().c_str());
  msg->add((int) inputsCount);
  sendOsc(msg, serverIP, targetPort);
  delete(msg);
}

int calibrate(int sensorNumber) {
  int sum = 0; // on ESP8266 default int is 32bits, should be sufficient event for a large MEANCALIBRATION
  for (int i=0; i< MEANCALIBRATION; i++) {
    sum += mcp3204.readADC(sensorNumber);
  }
  return int(sum/MEANCALIBRATION);
}

//--------------------------------------------------------------

int constrainValue(long value, int channelNumber) {
  int mappedValue = constrain(map(value, parameters.calibMin[channelNumber], parameters.calibMax[channelNumber], 0, 1000),0,1000);
  if (mappedValue < DEADBAND) {return 0;}
  else {return mappedValue;}
}

void loadCalibFromEeprom(){
  EEPROM.get(0, parameters);
  debugPrint("EEPROMDATA : SSID=");debugPrint(parameters.mySSID);debugPrint(", PSK=");debugPrint(parameters.PSK);debugPrint(", filter=");debugPrint(parameters.filter);debugPrint(", tag=");debugPrintln(parameters.tag);
  for (int i=0; i<inputsCount; i++) {debugPrint("min #"+String(i)+"=");debugPrint(parameters.calibMin[i]);debugPrint(", max #"+String(i)+"=");debugPrintln(parameters.calibMax[i]);}
  if (parameters.tag == eepromTag) {
    debugPrintln("loaded eeprom data");
  } else { 
    debugPrintln("no data found in eeprom, using defaults min/max");
    for (int i=0; i<inputsCount; i++) {
      parameters.calibMin[i] = defaultCalibMin;
      parameters.calibMax[i] = defaultCalibMax;
    }
    parameters.filter = 0;
    parameters.tag = eepromTag;
    memcpy(parameters.PSK, defaultPSK, sizeof(defaultPSK[0])*WIFIMAXLENGTH);
    memcpy(parameters.mySSID, defaultSSID, sizeof(defaultSSID[0])*WIFIMAXLENGTH);
    writeCalibToEeprom();
    }
 }

void writeCalibToEeprom(){
  EEPROM.put(0,parameters);
  EEPROM.commit();
  debugPrintln("calibration written to eeprom");
  }

OSCMessage* getOscMessage(){
  int packetSize = udpserver.parsePacket();
  if (packetSize)
  {
    //Serial.printf("Received %d bytes from %s, port %d\n", packetSize, udpserver.remoteIP().toString().c_str(), udpserver.remotePort());
    int len = udpserver.read(incomingPacket, packetSize);
    if (len > 0)  {incomingPacket[len] = 0;}
    IPfromLastMessageReceived= udpserver.remoteIP();
    OSCMessage* msg = new OSCMessage();
    msg->fill((uint8_t*)incomingPacket, len);
    int size = msg->getAddress(incomingAddress);
    ESP.wdtFeed();
    yield();
    return msg;
  }
  return NULL;
}

void sendOsc(OSCMessage *msg,IPAddress ip,int port ){
    udpserver.beginPacket(ip, port);
    msg->send(udpserver);
    udpserver.endPacket();
    ESP.wdtFeed();
    yield();
}

void connectToWifi(const char *Hostname, const char* ssid, const char* passphrase) {
  while (wifiAttempts < MAXWIFIATTEMPTS &&  WiFi.waitForConnectResult() != WL_CONNECTED ) {
    debugPrintln("Connecting to " + String(ssid) + " : attempt #" + String(wifiAttempts));
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, passphrase);
    WiFi.hostname(Hostname);
    ESP.wdtFeed();
    yield();
    wifiAttempts += 1;
  }
  if (wifiAttempts >= MAXWIFIATTEMPTS) {
    debugPrintln("reverting to default SSID/PSK after " + String(wifiAttempts) + " attempts");
    memcpy(parameters.PSK, defaultPSK, sizeof(defaultPSK[0])*WIFIMAXLENGTH);
    memcpy(parameters.mySSID, defaultSSID, sizeof(defaultSSID[0])*WIFIMAXLENGTH);
    writeCalibToEeprom();
    delay(1000);
    ESP.restart();
  }
  else {
    ArduinoOTA.setPort(8266); //default OTA port
    ArduinoOTA.setHostname(Hostname);// No authentication by default, can be set with : ArduinoOTA.setPassword((const char *)"passphrase");
    ArduinoOTA.begin();
  }
}
